return {
    setup = function()
        vim.g.ycm_key_list_select_completion = { "<C-n>", "<Down>" }
        vim.g.ycm_key_list_previous_completion = { "<C-p>", "<Up>" }
        vim.g.SuperTabDefaultCompletionType = "<C-n>"
        vim.g.UltiSnipsExpandTrigger = "<Tab>"
        vim.g.UltiSnipsJumpForwardTrigger = "<Tab>"
        vim.g.UltiSnipsJumpBackwardTrigger = "<S-Tab>"
    end
}

