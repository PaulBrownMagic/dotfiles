return {
    setup = function()
        local map = vim.keymap.set
        map("n", "<leader>vv", ":Vifm<CR>", { silent = true, desc = "Open Vifm (File Manager)" })
        map("n", "<leader>vs", ":VsplitVifm<CR>", { silent = true, desc = "Open Vifm to Vertical Split" })
        map("n", "<leader>vS", ":SplitVifm<CR>", { silent = true, desc = "Open Vifm to Horizontal Split" })
        map("n", "<leader>vd", ":DiffVifm<CR>", { silent = true, desc = "Open Vifm Diff Mode" })
        map("n", "<leader>vt", ":TabVifm<CR>", { silent = true, desc = "Open Vifm to New Tab" })
    end
}

