return {
    setup = function()
        vim.g.airline_powerline_fonts = 1
        vim.g.airline_theme = "base16_gruvbox_dark_pale"
        vim.g["airline#extensions#tabline#enabled"] = 0
    end
}

