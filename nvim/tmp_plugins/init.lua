return {
        -- Colorschemes & Aesthetics
        { "flazz/vim-colorschemes" },
        { "felixhummel/setcolors.vim" },
        { "ellisonleao/gruvbox.nvim", priority = 1000, config = function () vim.cmd.colorscheme "gruvbox", opt = true},

        -- UI Enhancements
        { "mhinz/vim-startify" },
        { "vim-airline/vim-airline" },
        { "vim-airline/vim-airline-themes" },
        { "airblade/vim-gitgutter" },

        -- Snippets & Autocomplete
        { "SirVer/ultisnips" },
        { "ervandew/supertab" },

        -- Syntax Checking
        { "vim-syntastic/syntastic", event = "BufReadPost" },

        -- File Navigation
        { "vifm/vifm.vim", lazy = false },
        { "junegunn/fzf", build = "./install --bin" },
        { "junegunn/fzf.vim", cmd = { "Files", "Buffers" } },

        -- CSS Colors & Rainbow
        { "skammer/vim-css-color", ft = { "css", "scss", "html" } },
        { "luochen1990/rainbow" },

        -- Python Formatting
        { "psf/black", ft = "python", branch = "stable" },

        -- Writing Mode
        { "junegunn/goyo.vim", cmd = "Goyo" },

        -- Vue Support
        { "posva/vim-vue", ft = "vue" },

        -- Import other plugin configs
        require("plugins.which_key"),
        require("plugins.lazygit"),
        require("plugins.avante"),
    }
