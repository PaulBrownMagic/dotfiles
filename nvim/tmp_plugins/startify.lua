return {
    setup = function()
        vim.g.startify_lists = {
            { type = "dir", header = { "    Current Directory " .. vim.fn.pathshorten(vim.fn.getcwd()) } },
            { type = "files", header = { "    Recent Files" } }
        }
        vim.g.startify_session_delete_buffers = 1
        vim.g.startify_change_to_vcs_root = 1
        vim.g.startify_enable_special = 1
    end
}

