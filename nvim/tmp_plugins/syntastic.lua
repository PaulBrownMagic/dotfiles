return {
    setup = function()
        vim.opt.statusline:append("%#warningmsg#")
        vim.opt.statusline:append("%{SyntasticStatuslineFlag()}")
        vim.opt.statusline:append("%*")

        vim.g.syntastic_always_populate_loc_list = 0
        vim.g.syntastic_auto_loc_list = 1
        vim.g.syntastic_loc_list_height = 5
        vim.g.syntastic_check_on_open = 0
        vim.g.syntastic_check_on_wq = 0
        vim.g.syntastic_python_checkers = { "pylint" }
        vim.g.syntastic_tex_checkers = { "chktex -n 9" }
    end
}

