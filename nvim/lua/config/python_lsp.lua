require("mason").setup {
  ensure_installed = { "black", "mypy" },
}
require("mason-lspconfig").setup {
  ensure_installed = { "pyright" },
}

-- Setup black on save

local null_ls = require("null-ls")

null_ls.setup {
  sources = {
    null_ls.builtins.formatting.black.with {
      extra_args = { "--line-length", "80" },
    },
	null_ls.builtins.diagnostics.mypy,
  },
}
-- -- Black (Python) --
vim.g.black_linelength = 79

-- Setup LSP
local lspconfig = require("lspconfig")

lspconfig.pyright.setup {
  on_attach = function(client, bufnr)
    -- Optional: Add keymaps or custom behavior here
    vim.api.nvim_buf_set_option(bufnr, "formatexpr", "v:lua.vim.lsp.formatexpr()")
	vim.api.nvim_create_autocmd("BufWritePre", {
		buffer = bufnr,
		callback = function()
		  vim.lsp.buf.format { async = false }
		end,
	  })
  end,
  settings = {
    python = {
      analysis = {
        autoSearchPaths = true,
        useLibraryCodeForTypes = true,
        diagnosticMode = "workspace",
      },
    },
  },
}

