-- Load core configurations
require('config.options')
require('config.keymaps')
require('config.autocmds')
require('config.digraphs')
require('config.neovide')
require('config.lazy')
require('config.python_lsp')
require('config.js_lsp')
