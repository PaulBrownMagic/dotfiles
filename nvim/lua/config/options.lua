-- Basic Settings
vim.g.mapleader = " "
vim.g.maplocalleader = ","

vim.opt.compatible = false
vim.cmd("filetype plugin on")
vim.cmd("filetype indent on")
vim.opt.encoding = "utf-8"
vim.opt.path:append("**")

-- General Editor Settings
vim.opt.syntax = "on"
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.hidden = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.expandtab = false
vim.opt.wrap = false
vim.opt.hlsearch = false
vim.opt.incsearch = true
vim.opt.scrolloff = 8
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.wildmenu = true
vim.opt.foldenable = false
vim.opt.wildmode = "longest,list,full"

-- Aesthetics & UI
-- vim.g.limelight_conceal_ctermfg = "gray"
-- vim.g.goyo_width = 81
vim.opt.colorcolumn = ""
