-- Mason setup for tools
require("mason").setup {
  ensure_installed = { "prettier", "eslint_d", "typescript-language-server" }, -- Same as JS, no extra tools needed yet
}
require("mason-lspconfig").setup {
  ensure_installed = { "ts_ls", "volar" }, -- Add volar for Vue
}

-- Setup null-ls for formatting and linting
local null_ls = require("null-ls")

null_ls.setup {
  sources = {
    null_ls.builtins.formatting.prettier.with {
      extra_args = { "--print-width", "80" },
      filetypes = { "javascript", "typescript", "vue" }, -- Add vue here
    },
    null_ls.builtins.diagnostics.eslint_d.with {
      extra_args = { "--cache" },
      filetypes = { "javascript", "typescript", "vue" }, -- Add vue here
    },
  },
}

-- Setup LSP with ts_ls (for JS/TS)
local lspconfig = require("lspconfig")

lspconfig.ts_ls.setup {
  on_attach = function(client, bufnr)
    vim.api.nvim_buf_set_option(bufnr, "formatexpr", "v:lua.vim.lsp.formatexpr()")
    vim.api.nvim_create_autocmd("BufWritePre", {
      buffer = bufnr,
      callback = function()
        vim.lsp.buf.format { async = false }
      end,
    })
  end,
  settings = {
    javascript = { format = { enable = false } },
    typescript = { format = { enable = false } },
  },
  filetypes = { "javascript", "javascriptreact", "typescript", "typescriptreact" },
}

-- Setup LSP with volar (for Vue)
lspconfig.volar.setup {
  on_attach = function(client, bufnr)
    vim.api.nvim_buf_set_option(bufnr, "formatexpr", "v:lua.vim.lsp.formatexpr()")
    vim.api.nvim_create_autocmd("BufWritePre", {
      buffer = bufnr,
      callback = function()
        vim.lsp.buf.format { async = false }
      end,
    })
  end,
  filetypes = { "vue" },
  init_options = {
    typescript = {
      -- Point to your global or project-specific TS server
      tsdk = "/home/pbrown/Applications/node-v18.16.0-linux-x64/lib/node_modules/typescript/lib", -- Adjust this path!
    },
  },
}
