local digraphs = {
    { "D=", "8784" },
    { "[C", "8847" },
    { "]C", "8848" },
    { "[_", "8849" },
    { "]_", "8850" },
    { "[U", "8851" },
    { "]U", "8852" },
    { "T-", "8868" },
    { "NE", "8708" },
}

for _, d in ipairs(digraphs) do
    vim.cmd(string.format("digraph %s %s", d[1], d[2]))
end

