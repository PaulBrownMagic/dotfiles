local autocmd = vim.api.nvim_create_autocmd

-- File Type Settings
autocmd({ "BufRead", "BufNewFile" }, {
    pattern = "*.pl",
    command = "set filetype=prolog"
})

autocmd({ "BufRead", "BufNewFile" }, {
    pattern = "calcurse-note.*",
    command = "set filetype=org"
})

autocmd({ "BufNewFile", "BufRead" }, {
    pattern = "*.pml",
    command = "set filetype=plantuml"
})

