-- Neovide Settings
vim.opt.guifont = "FiraCode Nerd Font Mono:h16"
vim.g.neovide_cursor_animation_length = 0.05
vim.g.neovide_cursor_trail_length = 0.8
vim.g.neovide_cursor_vfx_mode = "pixiedust"
vim.opt.mouse = "a"
