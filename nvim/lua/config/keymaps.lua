local map = vim.keymap.set

-- Movement
map("n", "0", "g0", { silent = true, desc = "Move to Beginning of Line" })
map("n", "$", "g$", { silent = true, desc = "Move to End of Line" })

-- Spell Toggle
map("n", "<leader>ss", ":setlocal spell! spelllang=en_gb<CR>", { silent = true, desc = "Toggle Spell Check" })

-- Buffers & Files
map("n", "<leader>bb", ":ls<CR>:b ", { silent = true, desc = "List Buffers & Switch" })
map("n", "<leader>bn", ":bnext<CR>", { silent = true, desc = "Next Buffer" })
map("n", "<leader>bp", ":bprev<CR>", { silent = true, desc = "Previous Buffer" })
map("n", "<leader>bd", ":bdelete<CR>", { silent = true, desc = "Delete Current Buffer" })
map("n", "<leader>,", ":Buffers<CR>", { silent = true, desc = "Fuzzy Find Buffers" })
map("n", "<leader>.", ":execute 'Files ' .. expand('%:p:h')<CR>", { silent = true, desc = "Fuzzy Find in Current Dir" })

-- Toggle Tabs/Spaces
map("n", "<leader>bt", ":set noexpandtab | retab!<CR>", { silent = true, desc = "Use Tabs for Indent" })
map("n", "<leader>bs", ":set expandtab | retab!<CR>", { silent = true, desc = "Use Spaces for Indent" })

-- Build & Run Commands
map("n", "<leader>mm", ":w<CR>:make<CR>", { silent = true, desc = "Save & Run Make" })
map("n", "<leader>mv", ":w<CR>:make<CR>:!xdg-open %&<CR>", { silent = true, desc = "Make & Open Output" })
map("n", "<leader>vv", ":!xdg-open %&<CR>", { silent = true, desc = "Open Current File" })

map("n", "<leader>xs", ":source %<CR>", { desc = "Source" })
map("n", "<leader>xl", ":.lua<CR>", { desc = "Run config Lua" })
map("v", "<leader>xl", ":lua<CR>", { desc = "Run config Lua" })

-- Terminal Mode
map("t", "<Esc>", "<C-\\><C-n>", { silent = true })
map("n", "<leader>tn", ":split | resize 15 | terminal<CR>i", { silent = true, desc = "Open Bottom Terminal" })


