return {
  {
	"jose-elias-alvarez/null-ls.nvim", -- for lsp integration
	enabled = true,
	ft = { "python" }
  },
  {
	"neovim/nvim-lspconfig",
	enabled = true
  },
  {
    "williamboman/mason.nvim",
	enabled = true,
	opts = {
	  ensure_installed = {
		"pyright"
	  }
	}
  },
  {
   "williamboman/mason-lspconfig.nvim",
	enabled = true
  }
}
