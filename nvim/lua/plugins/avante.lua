return{
	{
	  "yetone/avante.nvim",
	  event = "VeryLazy",
	  lazy = false,
	  version = false, -- Set this to "*" to always pull the latest release version, or set it to false to update to the latest code changes.
	  opts = {
		-- add any opts here
		-- for example
		provider = "claude",
		claude = {
		  endpoint = "https://api.anthropic.com",
		  model = "claude-3-5-sonnet-20241022", -- your desired model (or use gpt-4o, etc.)
		  timeout = 30000, -- timeout in milliseconds
		  temperature = 0, -- adjust if needed
		  max_tokens = 4096,
		  -- reasoning_effort = "high" -- only supported for reasoning models (o1, etc.)
		},
		dual_boost = {
		  enabled = false, -- Disable dual-boost mode by default
		},
		behaviour = {
		  auto_suggestions = false,
		  auto_set_highlight_group = true,
		  auto_set_keymaps = true,
		  auto_apply_diff_after_generation = false,
		  support_paste_from_clipboard = false,
		  minimize_diff = true,
		  enable_token_counting = true,
		  enable_cursor_planning_mode = false,
		},
		mappings = {
		  diff = {
			ours = "co",
			theirs = "ct",
			all_theirs = "ca",
			both = "cb",
			cursor = "cc",
			next = "]x",
			prev = "[x",
		  },
		  suggestion = {
			accept = "<M-l>",
			next = "<M-]>",
			prev = "<M-[>",
			dismiss = "<C-]>",
		  },
		  jump = {
			next = "]]",
			prev = "[[",
		  },
		  submit = {
			normal = "<CR>",
			insert = "<C-s>",
		  },
		  sidebar = {
			apply_all = "A",
			apply_cursor = "a",
			switch_windows = "<Tab>",
			reverse_switch_windows = "<S-Tab>",
		  },
		},
		windows = {
		  position = "right",
		  wrap = true,
		  width = 30,
		  sidebar_header = {
			enabled = true,
			align = "center",
			rounded = true,
		  },
		  input = {
			prefix = "> ",
			height = 8,
		  },
		  edit = {
			border = "rounded",
			start_insert = true,
		  },
		  ask = {
			floating = false,
			start_insert = true,
			border = "rounded",
			focus_on_apply = "ours",
		  },
		},
		highlights = {
		  diff = {
			current = "DiffText",
			incoming = "DiffAdd",
		  },
		},
		diff = {
		  autojump = true,
		  list_opener = "copen",
		  override_timeoutlen = 500,
		},
		suggestion = {
		  debounce = 600,
		  throttle = 600,
		},
	  },
	  -- if you want to build from source then do `make BUILD_FROM_SOURCE=true`
	  build = "make",
	  dependencies = {
		"nvim-treesitter/nvim-treesitter",
		"stevearc/dressing.nvim",
		"nvim-lua/plenary.nvim",
		"MunifTanjim/nui.nvim",
		--- The below dependencies are optional,
		"echasnovski/mini.pick", -- for file_selector provider mini.pick
		"nvim-telescope/telescope.nvim", -- for file_selector provider telescope
		"hrsh7th/nvim-cmp", -- autocompletion for avante commands and mentions
		"ibhagwan/fzf-lua", -- for file_selector provider fzf
		"nvim-tree/nvim-web-devicons", -- or echasnovski/mini.icons
		"zbirenbaum/copilot.lua", -- for providers='copilot'
		{
		  -- support for image pasting
		  "HakonHarnes/img-clip.nvim",
		  event = "VeryLazy",
		  opts = {
			-- recommended settings
			default = {
			  embed_image_as_base64 = false,
			  prompt_for_file_name = false,
			  drag_and_drop = {
				insert_mode = true,
			  },
			  -- required for Windows users
			  use_absolute_path = true,
			},
		  },
		},
		{
		  -- Make sure to set this up properly if you have lazy=true
		  'MeanderingProgrammer/render-markdown.nvim',
		  opts = {
			file_types = { "markdown", "Avante" },
		  },
		  ft = { "markdown", "Avante" },
		},
	  },
	}
}
