return {
	{ "vifm/vifm.vim",
	   enable = true,
	   lazy = false,
	   keys = {
		   { "<leader>vv", ":Vifm<CR>", silent = true, desc = "Open Vifm (File Manager)" },
		   { "<leader>vs", ":VsplitVifm<CR>", silent = true, desc = "Open Vifm to Vertical Split" },
		   { "<leader>vS", ":SplitVifm<CR>", silent = true, desc = "Open Vifm to Horizontal Split" },
		   { "<leader>vd", ":DiffVifm<CR>", silent = true, desc = "Open Vifm Diff Mode" },
		   { "<leader>vt", ":TabVifm<CR>", silent = true, desc = "Open Vifm to New Tab" },
	   }
   }
 }
