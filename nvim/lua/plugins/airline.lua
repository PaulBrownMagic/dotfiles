return {
  {	"vim-airline/vim-airline",
	enabled = true,
	config = function ()
	  vim.g.airline_powerline_fonts = 1
      vim.g["airline#extensions#tabline#enabled"] = 0
	end
  },
  { "vim-airline/vim-airline-themes",
	enabled = true,
	config = function ()
      vim.g.airline_theme = "base16_gruvbox_dark_pale"
	end
  }
}
