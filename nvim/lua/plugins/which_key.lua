return{
	{
        "folke/which-key.nvim",
        event = "VeryLazy",
        opts = {},
        config = function()
			local wk = require("which-key")
			wk.setup()
            vim.keymap.set("n", "<leader>?", function()
                require("which-key").show({ global = false })
            end, { desc = "Buffer Local Keymaps (which-key)" })
			wk.add({
			  { "<leader>b", group = "Buffers" },
			  { "<leader>m", group = "Make & Run" },
			  { "<leader>t", group = "Terminal" },
			  { "<leader>v", group = "Vifm" },
			  { "<leader>s", group = "Spell" },
			  { "<leader>h", group = "GitGutter" },
			  { "<leader>a", group = "AI" },
			  { "<leader>x", group = "Execute" },
			})
        end
    }
}

