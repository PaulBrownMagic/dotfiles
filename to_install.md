# To Install

A few apps to live by:

calcurse
irssi
logtalk
0AD
bsdgames-nonfree (rogue)
compton
nitrogen
klavaro
gimp
brave
OBS Studio
Open Shot
urxvt
nmtui (network manager ncurses)

Manual installs:

swi-prlog (from src)
graphdb
pyradio (and deps 1 & vlc)
Protege
qmk_firmware
node
eclipse
gprolog
graphviz (>=2.42.2)
Widoco

