from subprocess import run

DESKTOP_COUNT = 9
MONITOR_NAMES = ["PRIMARY", "SECONDARY", "TERTIARY", "QUATERNARY", "QUINARY", "SENARY"]

connected = run(['xrandr | grep " connected"'], check=True, capture_output=True, shell=True, text=True).stdout.split('\n')[:-1]
monitors = [o.split()[0] for o in connected]

def bspc(cmd):
    print(" ".join(cmd))
    run(cmd)

for i, m in enumerate(monitors[:-1]):
    cmdd = ["bspc", "monitor", m, "-d", str(i + 1)]
    cmdn = ["bspc", "monitor", m, "-n", MONITOR_NAMES[i]]
    bspc(cmdd)
    bspc(cmdn)

remaining = list(map(lambda i: str(i), range(len(monitors), DESKTOP_COUNT + 1)))
cmd = ["bspc", "monitor", monitors[-1], "-d"] + remaining
bspc(cmd)
cmd = ["bspc", "monitor", monitors[-1], "-n", MONITOR_NAMES[len(monitors) - 1]]
bspc(cmd)

cmd = ["bspc", "config", "-m", "^1", "top_padding", "18"]
bspc(cmd)

for i in range(1, len(monitors)):
    cmd = ["bspc", "config", "-m", f"^{i+1}", "top_padding", "0"]
    bspc(cmd)
