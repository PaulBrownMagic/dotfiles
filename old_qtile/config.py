import os
from pathlib import Path
from typing import List  # noqa: F401
import subprocess

from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget
from libqtile import hook

from applications import TERMINAL, BROWSER

WORKING_DIRECTORY = Path(os.path.dirname(os.path.realpath(__file__)))
TERMINAL_APP = str(TERMINAL + " -e {}").format

volume_up = "amixer -q -c 0 sset Master 5dB+"
volume_down = "amixer -q -c 0 sset Master 5dB-"

theme = {
    "name": "LondonTube Dark",
    "author": "",
    "color": [
        "#231f20",  # 0 black
        "#ee2e24",  # 1 red
        "#00853e",  # 2 green
        "#ffd204",  # 3 yellow
        "#009ddc",  # 4 blue
        "#98005d",  # 5 purple
        "#85cebc",  # 6 teal
        "#d9d8d8",  # 7 white
        "#737171",  # 8 grey
        "#ee2e24",  # 9 red
        "#00853e",  # 10 green
        "#ffd204",  # 11 yellow
        "#009ddc",  # 12 blue
        "#98005d",  # 13 purple
        "#85cebc",  # 14 teal
        "#ffffff",  # 15 white
    ],
    "foreground": "#d9d8d8",
    "background": "#231f20",
}


# mod = "mod4"
mod = "mod1"
# alt = "mod1"
shift = "shift"
ctrl = "control"
tab = "Tab"
space = "space"
up = "Up"
down = "Down"
left = "Left"
right = "Right"

keys = [
    # Switch between windows in current stack pane
    Key([mod], "m", lazy.layout.left()),
    Key([mod], "n", lazy.layout.down()),
    Key([mod], "e", lazy.layout.up()),
    Key([mod], "i", lazy.layout.right()),
    # Switch window focus to other pane(s) of stack
    # Key([alt], tab, lazy.layout.next()),
    # Move windows up or down in current stack
    Key([mod, shift], "m", lazy.layout.swap_left()),
    Key([mod, shift], "n", lazy.layout.shuffle_down()),
    Key([mod, shift], "e", lazy.layout.shuffle_up()),
    Key([mod, shift], "i", lazy.layout.swap_right()),
    # Change window sizes in Monads
    Key([mod, ctrl], "n", lazy.layout.normalize()),
    Key([mod, ctrl], "m", lazy.layout.maximize()),
    Key([mod, ctrl], "g", lazy.layout.grow()),
    Key([mod, ctrl], "s", lazy.layout.shrink()),
    # Key([mod], "l", lazy.to_screen(0)),
    # Key([mod], "u", lazy.to_screen(1)),
    Key([mod], tab, lazy.next_screen()),
    # Swap panes of split stack
    # Key([mod, "shift"], "space", lazy.layout.rotate()),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    # Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
    # Toggle between different layouts as defined below
    Key([mod], "k", lazy.next_layout()),
    Key([mod], "x", lazy.window.kill()),
    Key([mod, ctrl], "r", lazy.restart()),
    Key([mod, ctrl], "q", lazy.shutdown()),
    Key([mod], "r", lazy.spawncmd()),
    # Sound
    Key([ctrl], up, lazy.spawn(volume_up)),
    Key([ctrl], down, lazy.spawn(volume_down)),
    # Launchers (left-hand block: x taken to kill, z, s and h un-assigned
    Key([mod], "Return", lazy.spawn(TERMINAL)),
    Key([mod], "q", lazy.spawn(BROWSER)),
    Key([mod], "w", lazy.spawn("rofi -show drun")),
    Key([mod], "d", lazy.spawn("rofi -show run")),
    Key([mod], "a", lazy.spawn(TERMINAL_APP("nvim"))),
    Key([mod], "f", lazy.spawn(TERMINAL_APP("vifmrun"))),  # file Manager
    Key([mod], "r", lazy.spawn(TERMINAL_APP("pyradio"))),  # radio
    Key([mod], "c", lazy.spawn(TERMINAL_APP("calcurse"))),  # todo
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key([mod], i.name, lazy.group[i.name].toscreen()),
            # mod1 + shift + letter of group =
            #   switch to & move focused window to group
            Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
        ]
    )

tiling_theme = dict(
    border_focus=theme["color"][1],
    border_normal=theme["color"][0],
    single_border_width=0,
)

layouts = [
    layout.MonadTall(**tiling_theme),
    layout.MonadWide(**tiling_theme),
    layout.Max(),
    layout.Bsp(**tiling_theme),
    # layout.Zoomy(**tiling_theme)
    # layout.Floating(
    # border_focus=theme["color"][1], border_normal=theme["color"][0]
    # ),
]

widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=4,
)
extension_defaults = widget_defaults.copy()

panel_icons = "/usr/share/icons/Papirus-Dark/24x24/panel"
notify_beep = "/home/paul/.config/qtile/notify.wav"


def mk_bar():
    return bar.Bar(
        [
            widget.GroupBox(
                active=theme["foreground"],
                this_current_screen_border=theme["color"][1],
                other_current_screen_border=theme["color"][1],
                this_screen_border="#404040",
            ),
            widget.Prompt(),
            widget.WindowName(foreground=theme["foreground"]),
            # widget.TextBox("default config", name="default"),
            widget.Notify(
                foreground=theme["foreground"],
                foreground_urgent=theme["color"][1],
                foreground_low=theme["color"][8],
                audiofile=notify_beep,
                default_timeout=60,
            ),
            # widget.CheckUpdates(
            # display_format="⎡{updates}⎤",
            # colour_no_updates=theme["color"][8],
            # colour_have_updates=theme["color"][7],
            # distro="Arch",
            # update_interval=3600,
            # ),  # Hourly
            widget.Systray(),
            widget.Sep(foreground=theme["foreground"]),
            widget.Clock(
                format="%a %d %b %Y - %H:%M", foreground=theme["foreground"]
            ),
            widget.Sep(foreground=theme["foreground"]),
            widget.CurrentLayoutIcon(
                scale=0.6, foreground=theme["foreground"]
            ),
        ],
        26,
        background=theme["background"],
    )


# autostarts
@hook.subscribe.startup_once
def autostart_once():
    autostart = WORKING_DIRECTORY.joinpath("autostart.sh")
    subprocess.run(["bash", str(autostart)])
    # processes = [
    # [
    # "xrandr",
    # "--output",
    # "DP1-1",
    # "--mode",
    # "1920x1080",
    # "--right-of",
    # "eDP1",
    # ],
    # [
    # "xrandr",
    # "--output",
    # "DP1-2",
    # "--mode",
    # "1920x1080",
    # "--right-of",
    # "DP1-1",
    # ],
    # ["unclutter", "--timeout=1", "--fork"],
    # ["nitrogen", "--restore"],
    # ]
    # for p in processes:
    # subprocess.Popen(p)


@hook.subscribe.startup
def autostart():
    subprocess.call(["killall", "nm-applet"])
    subprocess.call(["killall", "pa-applet"])
    processes = [
        ["pa-applet"],
        ["nm-applet"],
        ["xfce4-power-manager", "--restart"],
        ["caffeine", "-d"],
    ]
    for p in processes:
        subprocess.Popen(p)


screens = [Screen(), Screen(top=mk_bar()), Screen()]

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod],
        "Button3",
        lazy.window.set_size_floating(),
        start=lazy.window.get_size(),
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]


# Remaining vars...
dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        *layout.Floating.default_float_rules,
        # {"wmclass": "confirm"},
        # {"wmclass": "dialog"},
        # {"wmclass": "download"},
        # {"wmclass": "error"},
        # {"wmclass": "file_progress"},
        # {"wmclass": "notification"},
        # {"wmclass": "splash"},
        # {"wmclass": "toolbar"},
        # {"wmclass": "confirmreset"},  # gitk
        # {"wmclass": "makebranch"},  # gitk
        # {"wmclass": "maketag"},  # gitk
        # {"wname": "branchdialog"},  # gitk
        # {"wname": "pinentry"},  # GPG key password entry
        # {"wmclass": "ssh-askpass"},  # ssh-askpass
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
