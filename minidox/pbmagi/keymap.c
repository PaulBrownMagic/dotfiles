#include QMK_KEYBOARD_H

// #define PERMISSIVE_HOLD
#define TAPPING_FORCE_HOLD

// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.

enum layers {
    _MODDH,
    _LOWER,
    _RAISE,
    _ADJUST
};

enum custom_keycodes {
  MODDH = SAFE_RANGE,
  LOWER,
  RAISE,
  ADJUST,
};

// Defines for task manager and such
//#define CALTDEL LCTL(LALT(KC_DEL))
//#define TSKMGR LCTL(LSFT(KC_ESC))

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Mod-DH
 *
 * ,----------------------------------.           ,----------------------------------.
 * |   Q  |   W  |   F  |   P  |   B  |           |   J  |   L  |   U  |   Y  |   ;  |
 * |------+------+------+------+------|           |------+------+------+------+------|
 * |   A  |   R  |   S  |   T  |   G  |           |   M  |   N  |   E  |   I  |   O  |
 * |------+------+------+------+------|           |------+------+------+------+------|
 * |   Z  |   X  |   C  |   D  |   V  |           |   K  |   H  |   ,  |   .  |   '  |
 * `----------------------------------'           `----------------------------------'
 *                  ,--------------------.    ,------,-------------.
 *                  | Ctrl |LOWER | SHIFT|    | SPACE| RAISE| Alt  |
 *                  | (Tab)|      |(ENTR)|    |      |      | (Esc)|
 *                  `-------------|      |    |      |------+------.
 *                                |      |    |      |
 *                                `------'    `------'
 */


[_MODDH] = LAYOUT( \
  KC_Q,    KC_W,    KC_F,    KC_P,    KC_B,         KC_J,    KC_L,    KC_U,    KC_Y,    KC_SCLN,    \
  KC_A,    KC_R,    KC_S,    KC_T,    KC_G,         KC_M,    KC_N,    KC_E,    KC_I,    KC_O, \
  KC_Z,    KC_X,    KC_C,    KC_D,    KC_V,         KC_K,    KC_H,    KC_COMM, KC_DOT,  KC_QUOTE, \
      LCTL_T(KC_TAB), LOWER, LSFT_T(KC_ENTER),    KC_SPC, RAISE, LALT_T(KC_ESCAPE)                 \
),

/* Raise - Vim Inspired
*
* ,----------------------------------.           ,----------------------------------.
* |   ~  |  "   |  +   |  ^   |  $   |           | HOME | PGDN | PGUP |  END | DEL  |
* |------+------+------+------+------|           |------+------+------+------+------|
* |   @  |  _   |  /   |  ?   |  ⊞   |           |  ←   |   ↓  |   ↑  |   →  | TAB  |
* |------+------+------+------+------|           |------+------+------+------+------|
* |   |  |  &   |  #   |  !   |  +   |           |  <   |   }  |   {  |   >  | INS  |
* `----------------------------------'           `----------------------------------'
*                  ,--------------------.    ,---------------------.
*                 |BACKSPC|      |      |    |      |      |       |
*                 |       |      |      |    |      |      |       |
*                 `-------+------+      |    |      |------+-------.
*                                `------'    `------'
*/
[_RAISE] = LAYOUT( \
  KC_TILD, KC_DQUO,  KC_PLUS,  KC_CIRC,  KC_DLR,        KC_HOME, KC_PGDN, KC_PGUP, KC_END,   KC_DELETE,    \
  KC_AT,   KC_UNDS,  KC_SLASH, KC_QUES, KC_LGUI,        KC_LEFT, KC_DOWN, KC_UP,   KC_RIGHT, KC_TAB, \
  KC_PIPE, KC_AMPR,  KC_HASH,  KC_EXLM, KC_PLUS,        KC_LABK, KC_RCBR, KC_LCBR, KC_RABK,  KC_INS, \
                      KC_BSPC, _______, _______,     _______, _______, _______                    \
),

/* Lower - Math & Punctuation
*
* ,----------------------------------.           ,----------------------------------.
* |   `  |   {  |   }  |   ^  |   £  |           |   /  |   7  |   8  |   9  |   *  |
* |------+------+------+------+------|           |------+------+------+------+------|
* |   %  |   (  |   )  |   :  |   -  |           |   0  |   4  |   5  |   6  |   -  |
* |------+------+------+------+------|           |------+------+------+------+------|
* |   \  |   [  |   ]  |   =  |   +  |           |   .  |   1  |   2  |   3  |   +  |
* `----------------------------------'           `----------------------------------'
*                  ,--------------------.    ,---------------------.
*                 |       |      |      |    |      |      |BACKSPC|
*                 |       |      |      |    |      |      |       |
*                 `-------+------+      |    |      |------+-------.
*                                `------'    `------'
*/
[_LOWER] = LAYOUT( \
  KC_GRAVE,   KC_LCBR, KC_RCBR, KC_CIRC,  UC(0x00A3),      KC_SLASH, KC_7, KC_8, KC_9, KC_ASTR, \
  KC_PERCENT, KC_LPRN, KC_RPRN, KC_COLON, KC_MINUS,          KC_0,     KC_4, KC_5, KC_6, KC_MINUS, \
  KC_BSLASH,  KC_LBRC, KC_RBRC, KC_EQUAL, KC_PLUS,           KC_DOT,   KC_1, KC_2, KC_3, KC_PLUS, \
                           _______, _______, _______,      _______, _______, KC_BSPC
),

/*Adjust (Raise & Lower) - Functions
*
* ,----------------------------------.           ,----------------------------------.
* |      |      |      |      |      |           |      |  F7  |  F8  |  F9  | F12  |
* |------+------+------+------+------|           |------+------+------+------+------|
* |      |      |      |Alt+□ |  □   |           |      |  F4  |  F5  |  F6  | F11  |
* |------+------+------+------+------|           |------+------+------+------+------|
* | RESET|      |      |      |      |           |      |  F1  |  F2  |  F3  | F10  |
* `----------------------------------'           `----------------------------------'
*                  ,--------------------.    ,---------------------.
*                 |       |      |      |    |ENTER |      |       |
*                 |       |      |      |    |      |      |       |
*                 `-------+------+      |    |      |------+-------.
*                                `------'    `------'
*/
[_ADJUST] =  LAYOUT( \
  _______, _______, UC_M_LN, UC_M_WI, RALT(KC_DLR), _______,   KC_F7,   KC_F8,   KC_F9,   KC_F12, \
  _______, _______, _______, RALT(KC_APP), KC_APP,  _______,   KC_F4,   KC_F5,   KC_F6,   KC_F11, \
  RESET,   _______, _______, _______, _______,      _______,   KC_F1,   KC_F2,   KC_F3,   KC_F10, \
                    _______, _______, _______,      KC_ENTER,  _______, _______                    \
)
};

void persistant_default_layer_set(uint16_t default_layer) {
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case MODDH:
      if (record->event.pressed) {
        #ifdef AUDIO_ENABLE
          PLAY_SONG(tone_qwerty);
        #endif
        persistant_default_layer_set(1UL<<_MODDH);
      }
      return false;
      break;
    case LOWER:
      if (record->event.pressed) {
        layer_on(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case RAISE:
      if (record->event.pressed) {
        layer_on(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case ADJUST:
      if (record->event.pressed) {
        layer_on(_ADJUST);
      } else {
        layer_off(_ADJUST);
      }
      return false;
      break;
  }
  return true;
}
