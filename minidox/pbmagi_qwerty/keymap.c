#include QMK_KEYBOARD_H


// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.

enum layers {
    _QWERTY,
    _LOWER,
    _RAISE,
    _ADJUST
};

enum custom_keycodes {
  QWERTY = SAFE_RANGE,
  LOWER,
  RAISE,
  ADJUST,
};

// Defines for task manager and such
//#define CALTDEL LCTL(LALT(KC_DEL))
//#define TSKMGR LCTL(LSFT(KC_ESC))

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Qwerty
 *
 * ,----------------------------------.           ,----------------------------------.
 * |   Q  |   W  |   E  |   R  |   T  |           |   Y  |   U  |   I  |   O  |   P  |
 * |------+------+------+------+------|           |------+------+------+------+------|
 * |   A  |   S  |   D  |   F  |   G  |           |   H  |   J  |   K  |   L  |   '  |
 * |------+------+------+------+------|           |------+------+------+------+------|
 * |Ctrl/Z|Alt/X |GUI/C |   V  |   B  |           |   N  |   M  |   ,  | Alt/.|Ctrl/;|
 * `----------------------------------'           `----------------------------------'
 *                  ,--------------------.    ,------,-------------.
 *                  | Ctrl | LOWER|Shift |    | GUI  | RAISE| Alt  |
 *                  | (Tab)|      |(Entr)|    |(Spce)|      | (Esc)|
 *                  `-------------|      |    |      |------+------.
 *                                |      |    |      |
 *                                `------'    `------'
 */


[_QWERTY] = LAYOUT( \
  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,         KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    \
  KC_A,    KC_S,    KC_D,    KC_F,    KC_G,         KC_H,    KC_J,    KC_K,    KC_L,    KC_QUOTE, \
  LCTL_T(KC_Z), LALT_T(KC_X), LGUI_T(KC_C), KC_V, KC_B,         KC_N,    KC_M, KC_COMM, RALT_T(KC_DOT), RCTL_T(KC_SCLN), \
    LCTL_T(KC_TAB), LOWER, LSFT_T(KC_ENTER),       LGUI_T(KC_SPC), RAISE, LALT_T(KC_ESCAPE)                 \
),


/* Raise - Vim Inspired
*
* ,----------------------------------.           ,----------------------------------.
* |  ESC |  "   |  +   |  0   |   $  |           | HOME | PGDN | PGUP |  END | DEL  |
* |------+------+------+------+------|           |------+------+------+------+------|
* |  TAB |  Y   |  P   |  @   |   ?  |           |  ←   |   ↓  |   ↑  |   →  | INS  |
* |------+------+------+------+------|           |------+------+------+------+------|
* |   |  |  &   |  _   |  #   |   !  |           |  &   |   {  |   }  |   <  |  >   |
* `----------------------------------'           `----------------------------------'
*                  ,--------------------.    ,---------------------.
*                 |       |      |      |    |      |      |       |
*                 |       |      |      |    |      |      |       |
*                 `-------+------+      |    |      |------+-------.
*                                `------'    `------'
*/
[_RAISE] = LAYOUT( \
  KC_ESCAPE, KC_DQUO, KC_PLUS, KC_0, KC_DLR,      KC_HOME, KC_PGDN,    KC_PGUP, KC_END, KC_DELETE,    \
  KC_TAB,    KC_Y, KC_P, KC_PLUS,   KC_QUES,      KC_LEFT, KC_DOWN, KC_UP, KC_RIGHT, KC_INS, \
  KC_PIPE, KC_AMPR,  KC_UNDS, KC_NONUS_HASH, KC_EXLM,    KC_AMPR, KC_LCBR, KC_RCBR, KC_LABK,  KC_RABK, \
            _______, _______, _______,   _______, _______, _______                    \
),

/* Lower - Math & Punctuation
*
* ,----------------------------------.           ,----------------------------------.
* |   `  |   {  |   }  |   +  |   £  |           |   /  |   7  |   8  |   9  |   *  |
* |------+------+------+------+------|           |------+------+------+------+------|
* |   %  |   (  |   )  |   :  |   -  |           |   0  |   4  |   5  |   6  |   -  |
* |------+------+------+------+------|           |------+------+------+------+------|
* |   \  |   [  |   ]  |   =  |   ^  |           |   .  |   1  |   2  |   3  |   +  |
* `----------------------------------'           `----------------------------------'
*                  ,--------------------.    ,---------------------.
*                 |       |      |      |    | Shift|      |       |
*                 |       |      |      |    |(BkSp)|      |       |
*                 `-------+------+      |    |      |------+-------.
*                                `------'    `------'
*/
[_LOWER] = LAYOUT( \
  KC_GRAVE, KC_LCBR, KC_RCBR, KC_PLUS, UC(0x00A3),      KC_SLASH, KC_7, KC_8, KC_9, KC_ASTR, \
  KC_PERCENT, KC_LPRN, KC_RPRN, KC_COLON, KC_MINUS,     KC_0, KC_4, KC_5, KC_6, KC_MINUS, \
  KC_BSLASH, KC_LBRC, KC_RBRC, KC_EQUAL, KC_CIRC,       KC_DOT, KC_1, KC_2, KC_3,  KC_PLUS, \
                    _______, _______, _______,          RSFT_T(KC_BSPC), _______, _______                    \
),

/*Adjust (Raise & Lower) - Functions
*
* ,----------------------------------.           ,----------------------------------.
* |      |      |      |      |      |           |      |  F7  |  F8  |  F9  | F12  |
* |------+------+------+------+------|           |------+------+------+------+------|
* |      |      |      |      |      |           |      |  F4  |  F5  |  F6  | F11  |
* |------+------+------+------+------|           |------+------+------+------+------|
* | RESET|      |      |      |      |           |      |  F1  |  F2  |  F3  | F10  |
* `----------------------------------'           `----------------------------------'
*                  ,--------------------.    ,---------------------.
*                 |       |      |      |    |      |      |       |
*                 |       |      |      |    |      |      |       |
*                 `-------+------+      |    |      |------+-------.
*                                `------'    `------'
*/
[_ADJUST] =  LAYOUT( \
  _______, _______, _______, _______, _______,      _______,   KC_F1,   KC_F2,   KC_F3,   KC_F12, \
  _______, _______, _______, _______, _______,      _______,   KC_F4,   KC_F5,   KC_F6,   KC_F11, \
  RESET,   _______, _______, _______, _______,      _______,   KC_F7,   KC_F8,   KC_F9,   KC_F10, \
                    _______, _______, _______,      _______,  _______, _______                    \
)
};

void persistant_default_layer_set(uint16_t default_layer) {
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case QWERTY:
      if (record->event.pressed) {
        #ifdef AUDIO_ENABLE
          PLAY_SONG(tone_qwerty);
        #endif
        persistant_default_layer_set(1UL<<_QWERTY);
      }
      return false;
      break;
    case LOWER:
      if (record->event.pressed) {
        layer_on(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case RAISE:
      if (record->event.pressed) {
        layer_on(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case ADJUST:
      if (record->event.pressed) {
        layer_on(_ADJUST);
      } else {
        layer_off(_ADJUST);
      }
      return false;
      break;
  }
  return true;
}
