# Minidox Keyboard Program

Requires [qmk_firmware](https://github.com/qmk/qmk_firmware).

Put `pbmagi` user keyboard dir into
`qmk_firmware/keyboards/minidox/keymaps`.

From the `qmk_firmware` root directory:

*Needs to be root*

Build with:
```
make minidox:pbmagi
```

Flash with:
```
make minidox:pbmagi:avrdude
```

# Layout

## Workman
```
,----------------------------------.           ,----------------------------------.
|   Q  |   D  |   R  |   W  |   B  |           |   J  |   F  |   U  |   P  |   ;  |
|------+------+------+------+------|           |------+------+------+------+------|
|   A  |   S  |   H  |   T  |   G  |           |   Y  |   N  |   E  |   O  |   I  |
|------+------+------+------+------|           |------+------+------+------+------|
|     Z|    X |    M |   C  |   V  |           |   K  |   L  |   ,  |     .|     '|
`----------------------------------'           `----------------------------------'
                 ,--------------------.    ,------,-------------.
                 | Ctrl |LOWER | SHIFT|    | SPACE| RAISE| Alt  |
                 | (Tab)|      |(ENTR)|    |      |      | (Esc)|
                 `-------------|      |    |      |------+------.
                               |      |    |      |
                               `------'    `------'
```

## Raise - Vim Inspired

```
,----------------------------------.           ,----------------------------------.
|   ~  |  "   |  +   |  ^   |   $  |           | HOME | PGDN | PGUP |  END | DEL  |
|------+------+------+------+------|           |------+------+------+------+------|
|   @  |  /   |  _   |  ?   |      |           |  ←   |   ↓  |   ↑  |   →  | TAB  |
|------+------+------+------+------|           |------+------+------+------+------|
|   |  |  &   |  #   |  !   |      |           |  <   |   {  |   }  |   >  | INS  |
`----------------------------------'           `----------------------------------'
                 ,--------------------.    ,---------------------.
                |       |      |      |    |      |      |       |
                |       |      |      |    |      |      |       |
                `-------+------+      |    |      |------+-------.
                               `------'    `------'
```

## Lower - Math & Punctuation

```
,----------------------------------.           ,----------------------------------.
|   `  |   {  |   }  |   ^  |   £  |           |   /  |   7  |   8  |   9  |   *  |
|------+------+------+------+------|           |------+------+------+------+------|
|   %  |   (  |   )  |   :  |   -  |           |   0  |   4  |   5  |   6  |   -  |
|------+------+------+------+------|           |------+------+------+------+------|
|   \  |   [  |   ]  |   =  |   +  |           |   .  |   1  |   2  |   3  |   +  |
`----------------------------------'           `----------------------------------'
                 ,--------------------.    ,---------------------.
                |       |      |      |    |BKSPAC|      |       |
                |       |      |      |    |      |      |       |
                `-------+------+      |    |      |------+-------.
                               `------'    `------'
```

## Adjust (Raise & Lower) - Functions

```
,----------------------------------.           ,----------------------------------.
|      |      |      |      |      |           |      |  F7  |  F8  |  F9  | F12  |
|------+------+------+------+------|           |------+------+------+------+------|
|      |      |      |      |      |           |      |  F4  |  F5  |  F6  | F11  |
|------+------+------+------+------|           |------+------+------+------+------|
| RESET|      |      |      |      |           |      |  F1  |  F2  |  F3  | F10  |
`----------------------------------'           `----------------------------------'
                 ,--------------------.    ,---------------------.
                |       |      |      |    |      |      |       |
                |       |      |      |    |      |      |       |
                `-------+------+      |    |      |------+-------.
                               `------'    `------'
```
