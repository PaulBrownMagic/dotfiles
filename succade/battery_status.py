#!/usr/bin/env python3

import subprocess
import gruvbox as theme
import lemonformat as lemon

acpi = subprocess.check_output(["acpi"], text=True)

ICONS = dict(empty = "\uF244",  # < 5
             low   = "\uF243",  # < 20
             mid   = "\uF242",  # < 60
             high  = "\uF241",  # < 90
             full  = "\uF240")

def parse_info(info):
    info = info[11:].split(",")
    status = dict(state=info[0].strip().lower(),
                percentage=int(info[1].strip()[:-1]))
    if len(info) > 2:
        status["time"] = info[-1].split()[0].strip()
    return status

def choose_icon(battery):
    percentage = battery["percentage"]
    icon = "full"
    if percentage < 5:
        icon = "empty"
    elif percentage < 20:
        icon = "low"
    elif percentage < 60:
        icon = "mid"
    elif percentage < 90:
        icon = "high"
    return lemon.Field(ICONS[icon])

def colour_icon(icon, battery):
    colour = "fg"
    if battery["state"] == "full":
        colour = "yellow"
    percentage = battery["percentage"]
    if percentage < 2:
        colour = "red-light"
    elif percentage < 5:
        colour = "red"
    elif percentage < 10:
        colour = "yellow-light"
    icon.add_fg(theme.COLORS[colour])

def battery_indicator(info):
    status = parse_info(info)
    icon = choose_icon(status)
    colour_icon(icon, status)
    return icon

print(battery_indicator(acpi))
