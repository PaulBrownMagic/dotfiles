#!/usr/bin/env python3

import subprocess
import re

def get_sound_output():
    return re.search(r"'\w+'",
            subprocess.check_output(["amixer"], text=True).split('\n')[0]
            ).group(0)[1:-1]

if __name__ == "__main__":
    print(get_sound_output())
