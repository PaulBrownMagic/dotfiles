#!/usr/bin/env python3
import subprocess
import json
import lemonformat as lemon
import gruvbox as theme

def update_workspaces():
    monitors = get_monitors()
    desktops = get_desktops()
    focused_monitor = monitor_with_focus()
    print_desktops(focused_monitor, monitors, desktops)

def monitor_with_focus():
    query = lemon.BSPCquery(["-T", "-m", "focused"])
    results = query.output()
    return json.loads(results)['name']

def get_monitors():
    query = lemon.BSPCquery(["-M", "--names"])
    results = query.output().split()
    return lemon.lmap(get_monitor_info, results)

def get_desktops():
    query = lemon.BSPCquery(["-D", "--names"])
    desktops = query.output().split()
    return lemon.smap(get_desktop_info, desktops, key=lambda d: d['name'])

def get_desktop_info(desktop):
    query = lemon.BSPCquery(["-T", "-d", desktop])
    jsn = query.output()
    info = json.loads(jsn)
    return dict(name=desktop,
                id=info['id'],
                hasContent=info['root'] is not None
                )

def get_monitor_info(monitor):
    query = lemon.BSPCquery(["-T", "-m", monitor])
    jsn = query.output()
    info = json.loads(jsn)
    return dict(name=monitor,
                focused=info["focusedDesktopId"]
                )

# desktop_icons = {
        # 'Linux': '\uF31A',
        # 'Web': '\uFA9E',
        # 'Vim': '\uE62B',
        # 'Terminal': '\uF120',
        # 'Code': '\uE796',
        # 'Pi': '\uF315',
        # 'Calendar': '\uF073',
        # 'Mail': '\uF6EF',
        # 'Music': '\uF001'
        # }
# desktop_names = ['Linux', 'Web', 'Vim', 'Terminal', 'Code', 'Calendar', 'Mail', 'Music']

# def desktop_icon(name_n:str) -> str:
    # n = int(name_n)
    # if 0 < n < len(desktop_names):
        # name = desktop_names[n]
    # else:
        # name = 'Linux'
    # icon = desktop_icons[name]
    # return icon

desktop_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
def desktop_icon(name_n:str) -> str:
    n = int(name_n)
    if 0 < n < len(desktop_names):
        return desktop_names[n]
    return '\uF31A'

def print_desktops(focused_monitor, monitors, desktops):
    desktop_on_focused_monitor = list(filter(lambda m: m['name'] == focused_monitor, monitors))[0]['focused']
    desktops_on_monitors = list(map(lambda m: m['focused'], monitors))
    # monitor_colours = ["red", "blue", "yellow", "purple"]  # For different colours per monitor
    for desktop in desktops:
        name = lemon.Field(desktop_icon(desktop['name']))
        if desktop['hasContent']:
            name.add_fg(theme.COLORS["yellow-light"])
        else:
            name.add_fg(theme.COLORS["fg"])

        if desktop['id'] in desktops_on_monitors:
            lspacer = lemon.Field("┘")
            rspacer = lemon.Field("└")
        else:
            lspacer = lemon.Field("─")
            rspacer = lemon.Field("─")

        if desktop['id'] == desktop_on_focused_monitor:
            lspacer = lemon.Field("┤")
            rspacer = lemon.Field("├")

        lspacer.add_fg(theme.COLORS["bg2"])
        rspacer.add_fg(theme.COLORS["bg2"])
        print(lspacer, end="")
        print(name, end="")
        print(rspacer, end="")
    print()

if __name__ == '__main__':
    update_workspaces()
