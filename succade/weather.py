#!/usr/bin/env python3

import time
import difflib
import lemonformat as lemon
import gruvbox as theme

lemon.SPACER.add_fg(theme.COLORS["bg2"])

class DayTime:

    def __init__(self, dawn, sunset):
        self._dawn = dawn
        self._sunset = sunset
        self.dawn = self.time_tuple(dawn)
        self.sunset = self.time_tuple(sunset)
        self.now = self.time_tuple(time.localtime())

    def time_tuple(self, itime):
        if type(itime) == str:
            itime = time.strptime(itime, "%H:%M:%S")
        return (itime.tm_hour, itime.tm_min, itime.tm_sec)

    @property
    def is_day(self):
        return self.dawn <= self.now <=self.sunset

weather_icons = {
	"Moderate or heavy snow in area with thunder": dict(day="\uE365", night="\uE366"),
	"Patchy light snow in area with thunder": dict(day="\uE362", night="\uE364"),
	"Moderate or heavy rain in area with thunder": dict(day="\uE30F", night="\uE32A"),
	"Patchy light rain in area with thunder": dict(day="\uE30E", night="\uE337"),
	"Moderate or heavy showers of ice pellets": dict(day="\uE304", night="\uE32F"),
	"Light showers of ice pellets": dict(day="\uFA91", night="\uFA91"),
	"Moderate or heavy snow showers": dict(day="\uE31A", night="\uE31A"),
	"Light snow showers": dict(day="\uE30A", night="\uE329"),
	"Moderate or heavy sleet showers": dict(day="\uE3AD", night="\uE3AD"),
	"Light sleet showers": dict(day="\uE3AA", night="\uE3AC"),
	"Torrential rain shower": dict(day="\uE308", night="\uE333"),
	"Moderate or heavy rain shower": dict(day="\uE308", night="\uE333"),
	"Light rain shower": dict(day="\uE308", night="\uE333"),
	"Ice pellets": dict(day="\uE314", night="\uE314"),
	"Heavy snow": dict(day="\uE31A", night="\uE31A"),
	"Patchy heavy snow": dict(day="\uE30A", night="\uE329"),
	"Moderate snow": dict(day="\uE31A", night="\uE31A"),
	"Patchy moderate snow": dict(day="\uE30A", night="\uE329"),
	"Light snow": dict(day="\uE31A", night="\uE31A"),
	"Patchy light snow": dict(day="\uE30A", night="\uE329"),
	"Moderate or heavy sleet": dict(day="\uE3AD", night="\uE3AD"),
	"Light sleet": dict(day="\uE3AD", night="\uE3AD"),
	"Moderate or Heavy freezing rain": dict(day="\uE3AD", night="\uE3AD"),
	"Light freezing rain": dict(day="\uE3AD", night="\uE3AD"),
	"Heavy rain": dict(day="\uE318", night="\uE318"),
	"Heavy rain at times": dict(day="\uE308", night="\uE325"),
	"Moderate rain": dict(day="\uE318", night="\uE318"),
	"Moderate rain at times": dict(day="\uE318", night="\uE318"),
	"Light rain": dict(day="\uE318", night="\uE318"),
	"Patchy light rain": dict(day="\uE318", night="\uE318"),
	"Heavy freezing drizzle": dict(day="\uE3AD", night="\uE3AD"),
	"Freezing drizzle": dict(day="\uE3AD", night="\uE3AD"),
	"Light drizzle": dict(day="\uE308", night="\uE325"),
	"Patchy light drizzle": dict(day="\uE308", night="\uE325"),
	"Freezing fog": dict(day="\uE313", night="\uE313"),
	"Fog": dict(day="\uE303", night="\uE346"),
	"Blizzard": dict(day="\uE35E", night="\uE35E"),
	"Blowing snow": dict(day="\uE35F", night="\uE361"),
	"Thundery outbreaks in nearby": dict(day="\uE30F", night="\uE32A"),
	"Patchy freezing drizzle nearby": dict(day="\uE3AD", night="\uE3AD"),
	"Patchy sleet nearby": dict(day="\uE3AD", night="\uE3AD"),
	"Patchy snow nearby": dict(day="\uE30A", night="\uE329"),
	"Patchy rain nearby": dict(day="\uE318", night="\uE318"),
	"Mist": dict(day="\uFA90", night="\uFA90"),
	"Overcast": dict(day="\uE30C", night="\uE379"),
	"Cloudy": dict(day="\uE312", night="\uE312"),
	"Partly Cloudy": dict(day="\uFA94", night="\uE379"),
	"Clear/Sunny": dict(day="\uE30D", night="\uE32B"),
}

def get_icon(desc, daytime):
    dk = "day" if daytime.is_day else "night"
    try:
        icon = weather_icons[desc][dk]
    except KeyError:
        close = difflib.get_close_matches(desc, list(weather_icons.keys()), cutoff=0.1)
        if len(close) > 0:
            icon = weather_icons[close[0]][dk]
        else:
            icon = "\E372" # barometer
    return lemon.Field(icon)

def weather_str(weather):
    daytime = DayTime(weather[2], weather[3])
    icon = get_icon(weather[0], daytime)
    icon.add_fg(theme.COLORS["yellow"])
    return f"{icon}{lemon.SPACER}{weather[1].lstrip('+')}"

# curl 'wttr.in/Leeds?format=%C,%t,%D,%d' > /tmp/latest_weather.txt
# Setup the curl as a cron job
with open('/tmp/latest_weather.txt') as latest_weather_file:
    weather = latest_weather_file.readlines()[0].split(",")
    print(weather_str(weather))


