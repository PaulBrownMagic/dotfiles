#!/usr/bin/env python3

import re
import subprocess
import gruvbox as theme
import lemonformat as lemon

from sound_output import get_sound_output

amixed = subprocess.check_output(["amixer", "sget", get_sound_output()], text=True)
mute = re.search(r"\[on\]", amixed) is None
volume = int(re.search(r"\[(\d+)%\]", amixed).group(0)[1:-2])

icons = dict(mute = lemon.Field("\uFA80"),
             low  = lemon.Field("\uFA7E"),
             med  = lemon.Field("\uFA7F"),
             high = lemon.Field("\uFA7D"))

lemon.lmap(lambda i: i.add_fg(theme.COLORS["yellow"]), icons.values())

if mute:
	print(f"{icons['mute']}")
elif volume < 33:
	print(f"{icons['low']}")
elif volume < 66:
	print(f"{icons['med']}")
else:
	print(f"{icons['high']}")


