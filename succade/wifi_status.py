#!/usr/bin/env python3

import re
import subprocess
import gruvbox as theme
import lemonformat as lemon

from socket import gethostname

# wlp2s0 (x1) or IFCONFIG (pi)
INTERFACE_NAME = "wlan0" if gethostname() == "pi" else "wlp2s0"
IFCONFIG = subprocess.check_output(["ifconfig", INTERFACE_NAME], text=True)

is_up = re.search(r"UP", IFCONFIG) is not None # Turned On
is_running = re.search(r"RUNNING", IFCONFIG) is not None # Connecting
is_connected = re.search(r"netmask", IFCONFIG) is not None # Connected

wifi_off = lemon.Field("\uFAA9")
wifi_on = lemon.Field("\uFAA8")

def dress(icon, colour):
    icon.add_fg(theme.COLORS[colour])
    return icon

if not is_up:
    icon = dress(wifi_off, "yellow")
elif not is_running:
    icon = dress(wifi_on, "fg")
elif is_running:
    icon = dress(wifi_on, "yellow")
else:
    icon = dress(wifi_off, "red")

print(icon)
