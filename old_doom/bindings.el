;;; bindings.el -*- lexical-binding: t; -*-

;; Keybindings
;;
;; localleader for major mode binding
(setq doom-localleader-key ",")

;; Select Window
(map! :n "C-w <left>" 'evil-window-left)
(map! :n "C-w <right>" 'evil-window-right)
(map! :n "C-w <up>" 'evil-window-up)
(map! :n "C-w <down>" 'evil-window-down)
;;
;; Swap Window
(map! :n "C-w x" 'evil-window-rotate-upwards)
(map! :n "C-w X" 'evil-window-rotate-downwards)
(map! :n "C-w C-x" 'evil-window-rotate-upwards)
(map! :n "C-w C-X" 'evil-window-rotate-downwards)

;; Incr/Decr
(map! :n "C-a +" 'evil-numbers/inc-at-pt)
(map! :n "C-a -" 'evil-numbers/dec-at-pt)
;; (map! :n "^" 'evil-first-non-blank) ; Std Vim
;; (map! :n "^" 'doom/backward-to-bol-or-indent) ; swaps between first non blank and 0

;; Comment toggle
(map! :leader :desc "Comment Toggle" "c <SPC>" #'comment-line)
(map! :leader :desc "Comment Toggle" "t <SPC>" #'comment-line)

;; Whitspace toggle
(map! :leader :desc "WhiteSpace Toggle" "t W" #'whitespace-mode)

;; Term
(map! :leader :desc "Toggle vterm in current project" "t t" '+vterm/toggle)
(map! :leader :desc "Toggle vterm in current window" "t T" '+vterm/here)

;; Open file
(map! :leader :desc "Find file, open in other window" "f o" 'find-file-other-window)
(defun toggle-ranger ()
  "Open ranger if it's closed, close it if it's open"
  (interactive)
  (if (and
       (boundp 'ranger-buffer)
       (buffer-live-p ranger-buffer))
      (ranger-close)
      (ranger)))
(map! :leader :desc "Toggle Ranger" "t m" 'toggle-ranger)

;; Search sensitivity
(defun toggle-case-sensitivity ()
  "Make search case-sensitive or not (smart)"
  (interactive)
  (if (eq evil-ex-search-case 'sensitive)
      (setq evil-ex-search-case 'smart)
      (setq evil-ex-search-case 'sensitive)))
(map! :leader :desc "Toggle Case Sensitivity" "t s" 'toggle-case-sensitivity)
