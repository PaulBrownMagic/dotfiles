#!/usr/bin/env python3
from collections import UserString
from subprocess import check_output

def lmap(*args): return list(map(*args))
def smap(*args, **kwargs): return sorted(map(*args), **kwargs)

class Field(UserString):

    def __init__(self, value):
        super().__init__(value)

    def add_fi(self, index):
        self.markup("T", index)

    def add_fg(self, color):
        self.markup("F", color)

    def add_bg(self, color):
        self.markup("B", color)

    def add_ul(self, color):
        self.markup("U", color)

    def markup(self, letter, param):
        self.data = f"%{{{letter}{param}}}{self.data}%{{{letter}-}}"

SPACER = Field("─")

class BSPC:

    def __init__(self):
        self.cmd = ["bspc"]

    def append_cmd(self, field: str):
        self.cmd.append(field)

    def append_cmds(self, fields: list):
        self.cmd = self.cmd + fields

    def output(self):
        return check_output(self.cmd, text=True)

    def __repr__(self):
        return self.cmd

class BSPCquery(BSPC):

    def __init__(self, fields):
        super().__init__()
        self.append_cmd("query")
        self.append_cmds(fields)

