;;; lgt.el -*- lexical-binding: t; -*-
;; Logtalk & Prolog Modes

(autoload 'prolog-mode "prolog" "Major mode for editing Prolog programs." t)
(add-to-list 'auto-mode-alist '("\\.pl\\'" . prolog-mode))
(setq prolog-system 'swi)

(add-to-list 'load-path "/home/pbrown/logtalk3/coding/emacs/")
(autoload 'logtalk-mode "logtalk" "Major mode for editing Logtalk programs." t)
(add-to-list 'auto-mode-alist '("\\.lgt\\'" . logtalk-mode))
(add-to-list 'auto-mode-alist '("\\.logtalk\\'" . logtalk-mode))
(defun my-tab-indent-hook ()
  (setq-default c-basic-offset 4
                tab-width 4
                indent-tabs-mode t))
(defun my-lgt-hook ()
  (setq comment-start "%"
        comment-end "")
  (setq fill-column 160)
  (whitespace-mode))

(add-hook 'logtalk-mode-hook 'my-tab-indent-hook)
(add-hook 'logtalk-mode-hook 'my-lgt-hook)


(setq-default logtalk-snippets-author "Paul Brown")
