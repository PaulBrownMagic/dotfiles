;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

(setq doom-localleader-key ",")

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Paul Brown"
      user-mail-address "pbrown@permion.ai")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
(setq doom-font (font-spec :family "FiraCode Nerd Font" :size 16))
(setq doom-big-font (font-spec :family "FiraCode" :size 32))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-gruvbox)
(let ((alternatives '(;; "emacs-e-logo.png"
                      ;; "emacs-gnu-logo.png"
                      ;; "nuvola_emacs.png"
                      ;; "trancendent-gnu.png"
                      "doomEmacsGruvbox.svg")))
  (setq fancy-splash-image
        (concat doom-private-dir "splash/"
                (nth (random (length alternatives)) alternatives))))

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(after! org
  (setq org-directory "~/org/")
  (setq org-log-done 'time)
  ;; (setq org-superstar-headline-bullets-list '("⬤" "⦿" "⦾" "○" "•" "·" "┄" "╌" "―"))
  (setq org-superstar-headline-bullets-list '("⬤" "○" "•" "·" "┄" "╌" "―"))
  (setq org-plantuml-jar-path "/bin/plantuml")
  (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
)
;; (setq org-log-done 'note))
;; Org-habit
(use-package! org-habit
  :after org
  :config
  (setq org-habit-following-days 1
        org-habit-preceding-days 14
        org-habit-show-habits t
        org-habit-show-all-today t))

(use-package org
  :config
  (add-to-list 'org-file-apps '("\\.odp\\'" . "libreoffice.impress %s")))

(defun my-org-mode-setup ()
  (setq tab-width 4))  ;; Set the desired tab width here (e.g., 4)

(add-hook 'org-mode-hook 'my-org-mode-setup)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)
(setq display-line-numbers t)

;; Project Management
(setq projectile-project-search-path '("~/dev/"))

;; Modeline
;; (setq display-time-mode t)
(display-time-mode t)

;; _ are part_of words!
(add-hook 'after-change-major-mode-hook
          (lambda ()
            (modify-syntax-entry ?_ "w")))

;;
;; Case sensitivity (search)
(setq evil-ex-search-case 'sensitive)

;; Text wrapping
(add-hook 'text-mode-hook 'auto-fill-mode) ; Hard wraps
;; (add-hook 'text-mode-hook 'visual-line-mode) ; Soft wraps

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.


;; Load other config
(load! "lgt")
(load! "python")
(load! "bindings")
(setq-default dotspacemacs-configuration-layers '(plantuml))

