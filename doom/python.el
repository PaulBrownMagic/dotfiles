;;; python.el -*- lexical-binding: t; -*-

;; Settings for Python
;; (setq lsp-python-ms-python-executable-cmd "python")
(setq-hook! 'python-mode-hook +format-with-lsp nil)
