# AwesomeWM config

Requires [Awesome-Copycats](https://github.com/lcpz/awesome-copycats)

This can be installed from this root dir with:

```
make clone
```

Running
```
make
```
will create soft-links to the rc.lua and `my*` theme directories
