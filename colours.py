THEME = {
	"name": "LondonTube Dark",
	"colour": [
		"#231f20", # black
		"#ee2e24", # red
		"#00853e", # green
		"#ffd204", # yellow
		"#009ddc", # blue
		"#98005d", # purple
                "#85cebc", # teal
                "#d9d8d8", # white
                "#737171", # grey
		"#ee2e24", # red
		"#00853e", # green
		"#ffd204", # yellow
		"#009ddc", # blue
		"#98005d", # purple
                "#85cebc", # teal
                "#d9d8d8", # white
                ],
        "foreground": "#d9d8d8",
        "background": "#231f20"
        }

