# Install

Soft link directory to `$HOME/.config/vifm`
Provided `$HOME/.local/bin` is in `$PATH` prior to `.bashrc` or `.zshrc` being
sourced, then softlink `vifmrun` script into that bin.

*For Menu support, including Rofi:*
Softlink the desktop entry to `$HOME/.local/share/applications/vifmrun.desktop`
And softlink the icon to `$HOME/.icons/default/radio-icon.png`
